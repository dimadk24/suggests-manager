const api_url = 'https://api.vk.com/method/';
const api_version = 5.73;
// url = https://oauth.vk.com/authorize?client_id=6371236&display=page&redirect_uri=https://oauth.vk.com/blank.html&scope=groups,wall,offline&response_type=token&v=5.73
const access_token = '6b942085826d3c9f1a7a80144eef22f77485505eb195e5f67596eedd14cb3a0a455e4994c9982d6d2dc44';
const bottom = '<div class="bottom"><button class="delete-many">Удалить отмеченные</button><button class="post-all">Запостить все</button></div>';

$.ajax({
    url: api_url + 'groups.get',
    dataType: 'jsonp',
    data: {
        extended: 1,
        filter: 'moder',
        count: 1000,
        access_token: access_token,
        v: api_version
    },
    success: function (data) {
        const select = $('select#public-select');
        for (let i = 0; i < data.response.count; i++){
            const option = '<option value="-' + data.response.items[i].id + '">' + data.response.items[i].name + '</option>';
            select.append(option);
        }
        select.select2();
        loadSuggests('-' + data.response.items[0].id);
        $('#public-select').on('select2:select', function (e) {
            const owner_id = e.params.data.id;
            loadSuggests(owner_id);
        });
    },
    error: function (){
        console.log('error');
    }
});

$(document).on("click", "div.check-wrapper", function() {
    if ($(document).width() <= 640) {
        $(this).toggleClass("checked");
        let checkbox = $(this).children('input[type=checkbox]');
        if (checkbox.prop("checked"))
            checkbox.prop("checked", false);
        else
            checkbox.prop("checked", true);
    }
});

$(document).on("click", "input[type=checkbox]", function() {
    $(this).parent().toggleClass("checked");
});

$(document).on("click", "label", function() {
    const post = $(this).parent().parent();
    const p = $(this).children('p');
    if (p[0].innerHTML === 'Анон') {
        $(post).data("anon", false);
        p.fadeOut(150, function() {
            p.replaceWith('<p>С подписью</p>');
            p.fadeIn(150);
        });
    } else {
        $(post).data("anon", true);
        p.fadeOut(150, function() {
            p.replaceWith('<p>Анон</p>');
            p.fadeIn(150);
        });
    }
});

$(document).on("click", "label > input[type=checkbox]", function() {
    return false;
});

$(document).on("click", "button.delete", function() {
    const post = $(this).parent().parent();
    const id = post.data("id");
    const owner_id = $('select#public-select').select2('data')[0].id;

    $.ajax({
        url: api_url + 'wall.delete',
        dataType: 'jsonp',
        data: {
            owner_id: owner_id,
            post_id: id,
            access_token: access_token,
            v: api_version
        },
        success: function(data) {
            if (data.response === 1)
                post.slideUp(function() {
                    post.remove();
                });
        }
    });
});

$(document).on("click", "button.delete-many", function() {
    const posts = $('div#posts').children();
    for (let i = 0; i < posts.length; i++) {
        const post = $(posts[i]);
        const checked = $(post).children('div.check-wrapper').children('input').prop('checked');
        if (checked) {
            const id = $(post).data("id");
            const timer = setTimeout(function() {
                const owner_id = $('select#public-select').select2('data')[0].id;
                $.ajax({
                    url: api_url + 'wall.delete',
                    dataType: 'jsonp',
                    data: {
                        owner_id: owner_id,
                        post_id: id,
                        access_token: access_token,
                        v: api_version
                    },
                    success: function(data) {
                        if (data.response === 1) {
                            $(post).slideUp(function() {
                                $(post).remove();
                            });
                        }
                    }
                });
            }, 400);
        }
    }
});

$(document).on("click", "button.post-all", function() {
    const owner_id = $('select#public-select').select2('data')[0].id;
    $.ajax({
        url: api_url + 'wall.get',
        dataType: 'jsonp',
        data: {
            owner_id: owner_id,
            count: 100,
            filter: "postponed",
            access_token: access_token,
            v: api_version
        },
        success: function(data) {
            if (data.response.count === 0)
                $.ajax({
                    url: api_url + 'utils.getServerTime',
                    dataType: 'jsonp',
                    data: {
                        access_token: access_token,
                        v: api_version
                    },
                    success: function (data) {
                        postData(data.response, 1);
                    }
                });
            else {
                const post_time = data.response.items[data.response.items.length-1].date;
                postData(post_time, 2);
            }
        }
    });
});

const postData = function (given_time, inc) {
    let time = new Date(given_time * 1000);
    time.setHours(time.getHours() + inc, 0, 0);
    const posts = $('div#posts').children();
    let posted = 0;
    for (let i = 0; i < posts.length; i++) {
        const post = $(posts[i]);
        const signed = +(!$(post).data('anon'));
        const id = $(post).data('id');
        const vk_attachments = $(post).data('vk-attachments');
        const timer = setTimeout(function() {
            const owner_id = $('select#public-select').select2('data')[0].id;
            $.ajax({
                url: api_url + 'wall.post',
                dataType: 'jsonp',
                data: {
                    owner_id: owner_id,
                    from_group: 1,
                    signed: signed,
                    attachments: vk_attachments,
                    post_id: id,
                    mark_as_ads: 0,
                    publish_date: time.getTime() / 1000,
                    access_token: access_token,
                    v: api_version
                },
                success: function(data) {
                    if (data.response.post_id)
                        posted++;
                    if (posted === posts.length) {
                        swal({
                            type: "success",
                            title: 'Успешно',
                            text: 'Отложено постов: ' + posted
                        });
                        $('div.bottom').remove();
                    }
                }
            });
            time.setHours(time.getHours() + 1);
            post.slideUp(function () {
                post.remove();
            });
        }, 500);
    }
};

const loadSuggests = function (owner_id) {
    $.ajax({
        url: api_url + 'wall.get',
        dataType: 'jsonp',
        data: {
            owner_id: owner_id,
            filter: 'suggests',
            count: 100,
            access_token: access_token,
            v: api_version
        },
        success: function(data) {
            const posts_dom = $('#posts');
            posts_dom.empty();
            $('div.bottom').remove();
            let posts = data.response.items;
            if (data.response.count === 0) {
                const no_content = '<p class="no-content">Новых предложенных постов нету</p>';
                posts_dom.append(no_content);
            }
            else {
                let new_posts = [];
                for (let i = 0; i < posts.length; i++) {
                    let post = {
                        id: 0,
                        time: 0,
                        attachments: [],
                        vk_attachments: '',
                        text: ''
                    };
                    if ('attachments' in posts[i])
                    {
                        for (let inner_i = 0; inner_i < posts[i].attachments.length; inner_i++) {
                            const attachment_type = posts[i].attachments[inner_i].type;
                            post.vk_attachments += attachment_type + posts[i].attachments[inner_i][attachment_type].owner_id + '_' + posts[i].attachments[inner_i][attachment_type].id + ',';
                            if (posts[i].attachments[inner_i].type in post.attachments)
                                post.attachments[posts[i].attachments[inner_i].type]++;
                            else
                                post.attachments[posts[i].attachments[inner_i].type] = 1;
                        }
                        post.vk_attachments = post.vk_attachments.slice(0, -1);
                    }
                    post.id = posts[i].id;
                    post.time = posts[i].date;
                    post.text = posts[i].text;
                    new_posts.push(post);
                }
                posts = new_posts;
                for (i = 0; i < posts.length; i++) {
                    let attachments = '';
                    for (let key in posts[i].attachments) {
                        attachments += posts[i].attachments[key] + ' ' + key + ', ';
                    }
                    attachments = attachments.slice(0, -2);
                    if (attachments)
                        attachments = '<div class="attachments">' + attachments + '</div>';
                    if (posts[i].text)
                        posts[i].text = '<div class="text">' + posts[i].text + '</div>';
                    const checkbox = '<div class="check-wrapper"><input type="checkbox"></div>';
                    const anon_check = '<div class="anon-wrapper"><label><p>Анон</p><input type="checkbox"><i class="fas fa-exchange-alt"></i></label></div>';
                    const button = '<div class="button-wrapper"><button class="delete"><i class="far fa-trash-alt"></i></button></div>';
                    posts_dom.append('<div class="post" data-id="' + posts[i].id + '" data-anon="true" data-vk-attachments="' + posts[i].vk_attachments + '">' + checkbox + anon_check + posts[i].text + attachments + button + '</div>');
                    attachments = '';
                }
                $('div.page').append(bottom);
            }
        },
        error: function() {
            console.log('error')
        }
    });
};